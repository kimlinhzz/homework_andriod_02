package first.mymapzz.com.howework;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CallAcitivty extends AppCompatActivity {

    Button btnCall;
    EditText edNumber;
    String number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_acitivty);
        btnCall = findViewById(R.id.button);
        edNumber = findViewById(R.id.ed_phone);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                number = edNumber.getText().toString();

                if (number.matches("^[0-9]{8,11}$")) {
                    if ( number.charAt(1)  != '0' ) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        number = "+855" + number;
                        intent.setData(Uri.parse("tel:" + number));
                        startActivity(intent);
                    }else {
                        Toast.makeText(CallAcitivty.this, "Your number can not start with double zero(00)", Toast.LENGTH_SHORT).show();

                    }

                } else {
                    Toast.makeText(CallAcitivty.this, "Your phone number must be at least 9 numbers to 11 numbers", Toast.LENGTH_LONG).show();
                }
            }

        });


    }
}
