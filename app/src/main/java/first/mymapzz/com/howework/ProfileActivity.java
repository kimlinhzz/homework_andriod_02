package first.mymapzz.com.howework;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edName;
    Button btnDatePicker, btnSave;
    RadioGroup genderGroup;
    RadioButton genderButton;
    Spinner spinnerDeveloper;
    TextView textDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        registerView();
        btnDatePicker.setOnClickListener(this);
        btnSave.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        if (view == btnDatePicker) {
            showDailogDatePicker();
        }
        if (view == btnSave) {
            showDailogFragment();
        }
    }

    private void showDailogFragment() {

        TextView textName, textGender, textBirthDay, textSkill;

        //get value from spinner
        String getSkill, getGender, getBirthDay, getName;
        //get value from group radio
        int selectedId = genderGroup.getCheckedRadioButtonId();
        // find the radiobutton by returned id
        genderButton = findViewById(selectedId);

        getGender = genderButton.getText().toString();
        getBirthDay = textDate.getText().toString();
        getName = edName.getText().toString();
        getSkill = spinnerDeveloper.getSelectedItem().toString();

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_info);

        //init view for dialog

        textName = dialog.findViewById(R.id.text_name_user);
        textGender = dialog.findViewById(R.id.text_gender_user);
        textBirthDay = dialog.findViewById(R.id.text_birthday_user);
        textSkill = dialog.findViewById(R.id.text_skill_user);


        textName.setText(getName);
        textGender.setText(getGender);
        textBirthDay.setText(getBirthDay);
        textSkill.setText(getSkill);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        Button btnClose = dialog.findViewById(R.id.btn_cancel);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setAttributes(lp);
        dialog.show();

    }


    private void showDailogDatePicker() {

        DatePickerDialog datePickerDialog = new DatePickerDialog(ProfileActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Toast.makeText(ProfileActivity.this, "Test", Toast.LENGTH_SHORT).show();
                textDate.setText(i + "/" + (i1+1) + "/" + i2);
            }
        }, 1998, 0, 1);
        datePickerDialog.show();
    }

    private void registerView() {

        edName = findViewById(R.id.ed_name);
        btnDatePicker = findViewById(R.id.btn_date);
        btnSave = findViewById(R.id.btn_save);
        genderGroup = findViewById(R.id.radio_group_gender);
        spinnerDeveloper = findViewById(R.id.spinner_developer);
        textDate = findViewById(R.id.text_date);

    }
}
