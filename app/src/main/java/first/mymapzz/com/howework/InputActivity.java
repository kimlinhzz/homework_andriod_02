package first.mymapzz.com.howework;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InputActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnCancel, btnSave;
    EditText edTitle, edDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        registerView();
        btnSave.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        if (view == btnSave) {
            String title = edTitle.getText().toString();
            String desc = edDesc.getText().toString();
            if (isDataValid(title, desc)) {
                intent.putExtra("text_title", title);
                intent.putExtra("text_desc", desc);
                setResult(Activity.RESULT_OK, intent);
                finish();
            } else {
                Toast.makeText(InputActivity.this, "You forget to input something", Toast.LENGTH_SHORT).show();
            }
        }
        if (view == btnCancel) {
            Toast.makeText(InputActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
            setResult(Activity.RESULT_CANCELED, intent);
            finish();
        }


    }

    private boolean isDataValid(String text1, String text2) {

        if (text1.isEmpty() || text1.length() == 0 || text2.isEmpty() || text2.length() == 0) {
            return false;
        }
        return true;
    }

    private void registerView() {
        btnCancel = findViewById(R.id.btn_cancel);
        btnSave = findViewById(R.id.btn_save);
        edDesc = findViewById(R.id.ed_desc);
        edTitle = findViewById(R.id.ed_title);
    }
}
