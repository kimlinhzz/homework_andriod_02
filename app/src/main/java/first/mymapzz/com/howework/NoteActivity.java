package first.mymapzz.com.howework;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.app.NativeActivity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class NoteActivity extends AppCompatActivity {

    LinearLayout note_layout;
    GridLayout gridLayout;
    final static int REQUEST_CODE_FOR_DATA = 1111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        note_layout = findViewById(R.id.card_add);
        gridLayout = findViewById(R.id.layout_note);
        note_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NoteActivity.this, InputActivity.class);
                startActivityForResult(intent, REQUEST_CODE_FOR_DATA);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_FOR_DATA) {
            if (resultCode == Activity.RESULT_OK) {
                String title = data.getStringExtra("text_title");
                String desc = data.getStringExtra("text_desc");
                createCardView(title, desc);
            }
        }
    }

    private void createCardView(String title, String description) {
        GridLayout.LayoutParams param = new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f)
                , GridLayout.spec(GridLayout.UNDEFINED, 1f));
        param.width = 350;
        param.height = 400;
        LinearLayout linearLayout = new LinearLayout(NoteActivity.this);

        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        params2.gravity = Gravity.CENTER_HORIZONTAL;
        params1.gravity = Gravity.CENTER_HORIZONTAL;
        param.setMargins(10, 10, 10, 10);

        linearLayout.setPadding(25, 10, 25, 10);
        linearLayout.setBackgroundColor(Color.parseColor("#ffffff"));
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        //create text title
        TextView tvTitle = new TextView(NoteActivity.this);
        tvTitle.setTypeface(Typeface.DEFAULT_BOLD);
        tvTitle.setMaxLines(1);
        tvTitle.setEllipsize(TextUtils.TruncateAt.END);
        tvTitle.setTextSize(16);
        tvTitle.setText(title);

        //create text desc
        TextView tvDesc = new TextView(NoteActivity.this);
        tvDesc.setMaxLines(3);
        tvDesc.setEllipsize(TextUtils.TruncateAt.END);
        tvDesc.setText(description);

        linearLayout.addView(tvTitle, params1);
        linearLayout.addView(tvDesc, params2);
        gridLayout.addView(linearLayout, param);
    }

}
