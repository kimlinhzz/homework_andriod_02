package first.mymapzz.com.howework;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private FrameLayout gallaryLayout, contactLayout, noteLayout, callLayout, profileLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_home);
        registerView();
        gallaryLayout.setOnClickListener(this);
        contactLayout.setOnClickListener(this);
        noteLayout.setOnClickListener(this);
        callLayout.setOnClickListener(this);
        profileLayout.setOnClickListener(this);
    }

    private void registerView() {
        gallaryLayout = findViewById(R.id.layout_gallary);
        contactLayout = findViewById(R.id.layout_contact);
        noteLayout = findViewById(R.id.layout_notebook);
        callLayout = findViewById(R.id.layout_call);
        profileLayout = findViewById(R.id.layout_profile);
    }

    @Override
    public void onClick(View view) {
        if (view == gallaryLayout) {
            intentCustom(GallaryActivty.class);
        }
        if (view == contactLayout) {
            intentCustom(ContectActivty.class);
        }
        if (view == noteLayout) {
            intentCustom(NoteActivity.class);
        }
        if (view == callLayout) {
            intentCustom(CallAcitivty.class);
        }
        if (view == profileLayout) {
            intentCustom(ProfileActivity.class);
        }
    }

    private void intentCustom(Class tempClass) {
        Intent intent = new Intent(MainActivity.this, tempClass);
        startActivity(intent);

    }


}
